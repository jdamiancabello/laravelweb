<html>
    <head>
        <title>Sobre nosotros</title>
        <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
        <style>
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .content {
                text-align: center;
                display: inline-block;
            }
            .title {
                font-size: 96px;
                margin-bottom: 40px;
                text-decoration: underline;
            }
            .quote {
                font-size: 24px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

        </style>
    </head>
    <body>

<div class="flex-center position-ref full-height">
                <div class="top-right links">
                    <a href="{{ url('/') }}">Volver al inicio</a>
                </div>

            <div class="content">
                <div class="title">Gestión de la pista de padel online</div>
                <div>¡Bienvenidos al gestor de la pista de padel online de nuestra comunidad de montepinar!</div>
				<div>En vista de que solo Fina y Antonio usaban la pista de padel de nuestra comunidad nuestro presidente (Bruno Quiroga) ideó la idea de automatizar la reserva mediante una web online y una app Android las condiciones para la reserva son las siguientes:</div>
				<div>
					<ol>
						<li>Cada vecino podrá realizar una reserva diaria como máximo.</li>
						<li>Cada reserva tendrá una duración máxima de 2 horas.</li>
						<li>Se podrá reservar la pista con 7 días de antelación, como máximo.</li>
						<li>La pista estará disponible diariamente de 9 a 23 horas.</li>
					</ol>
				</div>
				<div>Espero que con esto todos podamos hacer un uso de la pista y que cese el rayamiento de coches en el parking (Sabemos que es usted Fina).</div>
            </div>
        </div>
    </body>
</html>

</html>
