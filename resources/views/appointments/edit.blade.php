@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Editar reserva
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('appointments.update', $appointment->id) }}">
        @method('PUT')
        @csrf
        <div class="form-group">
          <label for="day">Dia</label>
          <input type="date" required min="{{date('Y-m-d')}}" class="form-control" name="day" value={{ $appointment->day }} />
        </div>
        <div class="form-group">
          <label for="timefinish">Hora de inicio</label>
	  <input required type="number" min="9" max="22" class="form-control" name="timestart" value={{ $appointment->timestart }} />
        </div>
        <div class="form-group">
	  <label for="timefinish">Hora de fin (10 equivale a 10:59)</label>
          <input required type="number" min="10" max="22" class="form-control" name="timefinish" value={{ $appointment->timefinish }} />
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
        <a class="btn btn-secondary" href="{{ route('appointments.index') }}"> Cancelar</a>
      </form>
  </div>
</div>
@endsection
