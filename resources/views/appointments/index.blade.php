@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>

<div id="myappointments" style="width: 100%; padding: 30px;">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Listado reservas</h2>
        </div>
        <div class="pull-right" align="right">
                <a class="btn btn-success mr-4 mb-2" href="{{ route('appointments.create') }}"> Crear nueva reserva</a>
        </div>
    </div>
</div>

<div class="uper">
  @if (session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  @if (session()->get('error'))
    <div class="alert alert-danger">
      {{ session()->get('error') }}  
    </div><br />
  @endif

  <div style="width: 100%; overflow-x: scroll;">
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Numero reserva</td>
          <td>Dia</td>
          <td>Hora inicio</td>
          <td>Hora fin</td>
          <td colspan="2"></td>
        </tr>
    </thead>
    <tbody>
        @foreach($appointments as $appointment)
        <tr>
            <td>{{$appointment->id}}</td>
            <td>{{$appointment->day}}</td>
            <td>{{$appointment->timestart}}:00</td>
            <td>{{$appointment->timefinish}}:59</td>
            <td><a href="{{ route('appointments.edit',$appointment->id)}}" class="btn btn-primary">Editar reserva</a></td>
            <td>
                <form action="{{ route('appointments.destroy', $appointment->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Borrar reserva</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
  </div>
<div>
</div>
@endsection
