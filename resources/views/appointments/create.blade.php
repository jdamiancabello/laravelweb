@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div id="createappointment" style="width: 100%; padding: 30px;">
<div class="card uper">
  <div class="card-header">
    Crear una reserva
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('appointments.store') }}">
          <div class="form-group">
              @csrf
              <label for="day">Dia</label>
              <input type="date" min="{{date('Y-m-d')}}" required class="form-control" name="day"/>
          </div>
          <div class="form-group">
              <label for="timestart">Hora de inicio</label>
              <input required min="9" max="22" type="number" class="form-control" name="timestart"/>
          </div>
          <div class="form-group">
              <label for="timefinish">Hora de fin (10 equivale a 10:59)</label>
              <input min="10" required max="22" type="number" class="form-control" name="timefinish"/>
          </div>
          <button type="submit" class="btn btn-primary">Crear</button>
          <a class="btn btn-secondary" href="{{ route('appointments.index') }}"> Cancelar</a>
      </form>
  </div>
</div>
</div>
@endsection
