<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>New appointment created</title>
</head>
<body>
    <p>New appointment added at {{ $appointment->created_at }}:</p>
    <ul>
        <li>Day: {{ $appointment->day }}</li>
        <li>Start time: {{ $appointment->timestart }}:00</li>
        <li>Finish time: {{ $appointment->timefinish }}:59</li>
    </ul>
</body>
</html>
