@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bienvenido al gestor de reservas <?php echo Auth::user()->name; ?>.</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
<center>
		    <div class="content" style="width: 100%;">
                        <div class="title m-b-md">
                            La que se avecina gestor de reservas ©JDamianCabello  2019-<?php echo date("Y"); ?>
                        </div>

                        <div class="links">
                            <a class="btn btn-success" href="{{ route('appointments.create') }}" style="margin: 10px;"> Crear una nueva reserva</a>
                            <a class="btn btn-success" style="margin: 10px; font-color: white; background: #4fc3f7; border: 1px solid #4fc3f7;" href="{{ route('appointments.index') }}"> Mostrar reservas</a>
                        </div>
                    </div>
</center>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection