# Practica 4 CRUD LARAVEL
## Introducción

Proyecto basado en el Framework de Laravel con el proposito de realizar reservas de una pista de padel mediante una web. La reserva esta sujeta a unas condiciones explicadas en el punto Control de insercción de datos mas abajo. Tambien cuenta con una API mediante la cual haremos una comunicación con una App android la cual se puede ver [aquí](https://gitlab.com/jdamiancabello/laravelandroid).

## Informacion

Laravel es un Framework basado en PHP que permite crear aplicaciones y servicios en la web. Laravel implementa un patron MVC y el uso de plantillas (Blade) ofeciendo así un código más limpio y legible. También ofrece una sencilla forma de manejar la parte de base de datos con su [ORM](https://es.wikipedia.org/wiki/Mapeo_objeto-relacional) (Object Relational Mapping o mapeo objeto relacional en español) Eloquent, que permite de una forma muy simple crear todo tipo de consultas.

## Configuración necesaria

-   Un servidor con Nginx o Apache2.
-   Base de datos MySQL/MariaDB con la base de datos crudexampleDamian, y con el usuario `crudexampleDamian` contraseña `malaga2018`.
-   PHP.
-   PostFix en el servidor configurado.

## Estructura de directorios básicos en Laravel

**app:** Contiene todo el código de la aplicación.

**app/Console:** Contiene todos los comandos artisan para usar en la aplicación.

**app/Exceptions:** Contiene las clases para manejar excepciones.

**app/Http:** Almacena los controladores creados en la aplicación.

**bootstrap:** Contiene los archivos y código que usa boostrap.

**config:** Contiene los archivos de configuración.

**database:** Contiene las migraciones y archivos de creación de registros para pruebas.

**public:** Contiene archivos como imágenes, JavaScript CSS, etc.

**resources:** Ahí se encuentran todos los archivos y vistas de nuestra aplicación.

**routes:** Contiene las rutas para nuestra aplicación.


## Cambiar los mensajes de errores en validaciones a español

Por defecto los mensajes de error están en inglés, los puedes dejar en la configuración por defecto, pero si quieres cambiarlos a español debes ir al siguiente repositorio de **[github](https://github.com/MarcoGomesr/laravel-validation-en-espanol)** descargar y copiar la carpeta **“es”** en /resources/lang/ de tu proyecto, y luego en el archivo /config/app.php, debes buscar la línea:

    'locale' => 'en',

Y cambiar en por **es**, como se ve a continuación:

    'locale' => 'es',



## Configuración de conexión a la base de datos

Para esto debes ir al archivo [/config/database.php](https://gitlab.com/jdamiancabello/laravelweb/blob/master/config/database.php) buscar la zona de configuración para MySQL que debe estar como sigue:
A continuación debes cambiar las siguientes líneas:

    'host'  =>  env('DB_HOST',  'localhost'),  // servidor
    'port'  =>  env('DB_PORT',  '3306'),
    'database'  =>  env('DB_DATABASE',  'laracrud'),  //nombre de la bd
    'username'  =>  env('DB_USERNAME',  'root'),  //usuario
    'password'  =>  env('DB_PASSWORD',  'root'),  //clave
    'charset'  =>  'utf8',  //juego de caracteres
    'collation'  =>  'utf8_unicode_ci',  //juego de caracteres


Adicionalmente debes ir al archivo [.env](https://gitlab.com/jdamiancabello/laravelweb/blob/master/.env) y buscar las siguientes líneas:

    DB_CONNECTION=mysql // tipo de conexión que se realiza
    DB_HOST=127.0.0.1 // host hasta la base de datos (si es en el mismo server se queda así)
    DB_PORT=3306 // puerto de conexión, este es el de por defecto
    DB_DATABASE=homestead // nombre de nuestra base de datos
    DB_USERNAME=homestead // nombre de usuario para loguear a la base de datos
    DB_PASSWORD=secret // contraseña para loguear a la base de datos


## Creación de Migraciones

El tema de migraciones en Laravel tiene que ver con el diseño de tablas de nuestra base de datos, y aunque podemos directamente crear nuestra tabla en MySQL crear el controlador y el modelo, lo vamos hacer usando migraciones, **cual es la diferencia?** Pues la diferencia es que usando migraciones diseñas las tablas como si de modelos se tratarán y luego las puedes generar a MySQL con un sólo comando, además puedes llevar la cronología de la creación de tus tablas y si algo salió mal, por ejemplo te olvidaste de crear un campo, fácilmente haces un rollback y deshaces los cambios.

La tabla que vamos a crear para el ejemplo se va llamar `appointments` y contendrá los campos: id, timestamps, day, timestart, timefinish, userid. (Puedes ver el resultado [aqui](https://gitlab.com/jdamiancabello/laravelweb/blob/master/database/migrations/2019_03_01_114809_create_appointments_table.php)).

En ciertos casos puede que que quieras añadir algún campo a alguna tabla que ya está creada, simplemente puedes hacer un rollback, lo que hace en este caso es eliminar las tablas creadas con las migraciones, la base de datos queda vacía sin ninguna tabla, con el siguiente comando:

    php artisan migrate:rollback

## El modelo
Si vas a la carpeta app del proyecto vas a encontrar un nuevo archivo llamado Appointment.php, como te darás cuenta es una clase vacía y en teoría deberíamos crear las propiedades, los setter y getter, pero con la ayuda de Laravel sólo creamos un array $fillable y le pasamos los campos que queremos llenar, así de fácil como se muestra a continuación:

    class Appointment extends Model
    {
	     protected $fillable = ['day', 'timestart', 'timefinish', 'user_id'];
	     public function user()
     {
     return $this->belongsTo('App\User');
     
[Aqui](https://gitlab.com/jdamiancabello/laravelweb/blob/master/app/Appointment.php) tienes el ejemplo real por si quieres verlo.

## El Controlador
Laravel es un Framework que usa el patrón MVC (Modelo, Vista, Controlador), por lo que ahora tenemos que crear el controlador con los métodos index, show, update, delete.

Si ahora vas a la carpeta app/Http/Controllers puedes ver que se generó un nuevo archivo [AppointmentsController.php](https://gitlab.com/jdamiancabello/laravelweb/blob/master/app/Http/Controllers/AppointmentsController.php "AppointmentsController.php") y dentro del mismo estan todos los controles de CRUD en los distintos métodos.

## Crear las vistas

Finalmente lo que vamos hacer es crear las vistas, para mostrar, editar y eliminar reservas, para esto vamos a /resources/views/ y creamos una nueva carpeta llamada layouts y dentro de esta carpeta creamos el archivo [app.blade.php](https://gitlab.com/jdamiancabello/laravelweb/blob/master/resources/views/layouts/app.blade.php "app.blade.php"), este archivo tiene la plantilla del proyecto, es importante mencionar que Laravel usa el motor de plantillas blade que junto con HTML permiten crear vistas más simples y limpias.

Posteriormente en la ruta /resources/views/ creamos una nueva carpeta llamada [appointments](https://gitlab.com/jdamiancabello/laravelweb/tree/master/resources/views/appointments "appointments") que va contener las vistas (index, create, edit).

## Control de insercción de datos.
 **Normas de validación**

Las normas en las que me baso para realizar una reserva son las siguientes:

-   La reserva debe realizarse en máximo 7 días exactos. (Incluye hora)
-   La reserva no puede entrar dentro del rango de otra reserva.
-   La reserva dura máximo 2 horas.
-   La reserva solo está comprendida de 9:00 a 23:00.
-   Una reserva máxima diaria por persona. 

El control de los datos se hace directamente en la misma base de datos mediante triggers los cuales son los siguientes:

    --
    -- Disparadores `appointments`
    --
    DROP TRIGGER IF EXISTS `bi_appointments`;
    DELIMITER $$
    CREATE TRIGGER `bi_appointments` BEFORE INSERT ON `appointments` FOR EACH ROW begin
    
        declare msg varchar(128);
        
    if new.timestart > new.timefinish then
            set msg = '#ERROR: La hora de inicio no puede ser mayor que la hora final. AM@';
            signal sqlstate '47000' set message_text = msg;
        end if;
    
        if new.timestart < 9 then
            set msg = '#ERROR: La hora de inicio no puede ser inferior a 9 AM@';
            signal sqlstate '47000' set message_text = msg;
        end if;
    
        if new.timestart > 22 then
            set msg = '#ERROR: El tiempo de entrada no puede ser menor a 10 PM@';
            signal sqlstate '47001' set message_text = msg;
        end if;
    
        if new.timefinish > 22 then
            set msg = '#ERROR: El tiempo de llegada no puede ser mayor a 10:59 PM@';
            signal sqlstate '47002' set message_text = msg;
        end if;
    
        if DATE_ADD(now(), INTERVAL 7 DAY) < new.day then
            set msg = '#ERROR: No se puede reservar con más de 7 días de antelación@';
            signal sqlstate '47004' set message_text = msg;
        end if;
    
        if (curdate() > new.day or (curdate() = new.day and new.timestart < hour(now()))) then
            set msg = '#ERROR: No se puede reservar el pasado@';
            signal sqlstate '47005' set message_text = msg;
        end if;
    
        if EXISTS (SELECT day FROM appointments WHERE user_id = new.user_id and day = new.day) then
            set msg = '#ERROR: Solo puede haber una cita por usuario una vez al día, intente con otro día@';
            signal sqlstate '47006' set message_text = msg;
        end if;
    
        if (new.timefinish - new.timestart) > 1 then
            set msg = '#ERROR: No se permite reservar más de dos horas@';
            signal sqlstate '47007' set message_text = msg;
        end if;
    
        if EXISTS (SELECT * FROM appointments WHERE (timestart between new.timestart and new.timefinish and day = new.day)) then
            set msg = '#ERROR: Hay otra reserva dentro de este rango de tiempo en ese día';
            signal sqlstate '47006' set message_text = msg;
        end if;
    
        if EXISTS (SELECT * FROM appointments WHERE (new.timestart between timestart and timefinish and day = new.day)) then
            set msg = '#ERROR: Hay otra reserva dentro de este rango de tiempo en ese día';
            signal sqlstate '47006' set message_text = msg;
        end if;
    
    end
    $$
    DELIMITER ;
    DROP TRIGGER IF EXISTS `bu_appointments`;
    DELIMITER $$
    CREATE TRIGGER `bu_appointments` BEFORE UPDATE ON `appointments` FOR EACH ROW begin
    
        declare msg varchar(128);
        
    if new.timestart > new.timefinish then
            set msg = '#ERROR: La hora de inicio no puede ser mayor que la hora final.@';
            signal sqlstate '47000' set message_text = msg;
        end if;
    
        if new.timestart < 9 then
            set msg = '#ERROR: La hora de inicio no puede ser inferior a 9 AM@';
            signal sqlstate '47000' set message_text = msg;
        end if;
    
        if new.timestart > 22 then
            set msg = '#ERROR: La hora de inicio no puede ser superior a 10 PM@';
            signal sqlstate '47001' set message_text = msg;
        end if;
    
        if new.timefinish > 22 then
            set msg = '#ERROR: El tiempo de llegada no puede ser mayor a 10:59 PM@';
            signal sqlstate '47002' set message_text = msg;
        end if;
    
        if DATE_ADD(now(), INTERVAL 7 DAY) < new.day then
            set msg = '#ERROR: No se permiten reservas de mas de 7 días de antelación.@';
            signal sqlstate '47004' set message_text = msg;
        end if;
    
        if (curdate() > new.day or (curdate() = new.day and new.timestart < hour(now()))) then
            set msg = '#ERROR: No se puede registrar una reserva con fecha pasada.@';
            signal sqlstate '47005' set message_text = msg;
        end if;
    
        if EXISTS (SELECT day FROM appointments WHERE user_id = new.user_id and day = new.day and old.day <> new.day) then
            set msg = '#Date error: Solo puede haber una reserva por usuario una vez al día, intente con otro día@';
            signal sqlstate '47006' set message_text = msg;
        end if;
    
        if (new.timefinish - new.timestart) > 1 then
            set msg = '#ERROR: No puedes reservar más de 2 horas@';
            signal sqlstate '47007' set message_text = msg;
        end if;
    
        if EXISTS (SELECT * FROM appointments WHERE (timestart between new.timestart and new.timefinish and day = new.day)) then
            set msg = '#ERROR: Hay otra reserva dentro de este rango de tiempo en ese día';
            signal sqlstate '47006' set message_text = msg;
        end if;
    
        if EXISTS (SELECT * FROM appointments WHERE (new.timestart between timestart and timefinish and day = new.day)) then
            set msg = '#ERROR: Hay otra reserva dentro de este rango de tiempo en ese día';
            signal sqlstate '47006' set message_text = msg;
        end if;
    
    end
    $$
    DELIMITER ;
    
    -- --------------------------------------------------------
Ejemplo completo de la base de datos [aquí](https://gitlab.com/jdamiancabello/laravelweb/blob/master/crudexampleDamian.sql).

## API

---

Las llamadas a la API en cuanto a cuentas permite crear nuevos usuarios, obtener su información de cuenta y los tokens de autenticación. Por otro lado permiten realizar las operaciones CRUD propias de la entidad _Appointment_.

**Registro**
----
  Llamada que permite registrarse, devuelve un token con el cual podremos realizar las acciones sobre las diferentes clases.

* **URL**

  https://laravel.jdamiancabello.es/api/register

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
   `email=[alphanumeric]`
 
   `password=[alphanumeric]`

* **Data Params**

    *Headers* <br/>
    Content-Type application/x-www-form-urlencoded <br/>
    <br/> *Body* urlencoded <br/>
    name	testapi <br/>
    email	emailapi@email.com <br/>
    password	1234

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ "token": "eyJ0eXA...." }`
 
* **Error Response:**

  No required params
  ```
  {
    "error": {
        "name": [
            "The name field is required."
        ],
        "email": [
            "The email field is required."
        ],
        "password": [
            "The password field is required."
        ]
    }
  }
  ```

  OR

  Email taken
  ```
  {
    "error": {
        "email": [
            "The email has already been taken."
        ]
    }
  }
  ```

* **Sample Call:**

  ```
  curl --location --request POST "https://laravel.jdamiancabello.es/api/register" \
  --header "Content-Type: application/x-www-form-urlencoded" \
  --data "name=testapi&email=emailapi@email.com&password=1234"
  ``` 

**Login**
----

  Llamada de login que devolvera el token de seguridad.

* **URL**

  https://laravel.jdamiancabello.es/api/login

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
   `email=[alphanumeric]`
 
   `password=[alphanumeric]`

* **Data Params**

  *Headers* <br/>
  Accept	application/json <br/>
  Content-Type	application/json <br/>
  <br/> *Body* formdata <br/>
  email	email@email.com <br/>
  password	blabla

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ "token": "eyJ0eXA...." }`
 
* **Error Response:**

  Not valid account

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ "error": "Unauthorised" }`

* **Sample Call:**

  curl --location --request POST "https://laravel.jdamiancabello.es/api/login" \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --form "email=email@email.com" \
  --form "password=blabla"

**User**
----

  Get user data. 

* **URL**

  https://laravel.jdamiancabello.es/api/user

* **Method:**
  
  `GET`

* **Data Params**

    *Headers* <br/>
    Authorization Bearer eyJ0eX ... <br/>
    Accept application/json

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
  ```
  {
    "user": {
      "id": 1,
      "name": "x",
      "email": "x@mail.com",
      "email_verified_at": "2019-03-01 12:15:54",
      "created_at": "2019-03-01 12:15:44",
      "updated_at": "2019-03-01 12:15:54"
    }
  }
  ```

* **Sample Call:**

  ```
  curl --location --request GET "https://laravel.jdamiancabello.es/api/user" \
  --header "Authorization: Bearer eyJ0eXAiOiJKV ..." \
  --header "Accept: application/json" \
  --data ""
  ```

**Send email**
----

* **URL**

  https://laravel.jdamiancabello.es/api/email

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Data Params**

  *Body*
  ```
  {
    "to": "yo@jdamiancabello.es",
    "subject": "ejemplo",
    "message": "El mensaje fue escrito en laravel"
  }
  ```
  *Headers* </br>
  Content-Type application/json

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `"Email enviado"`

**List appointments**
----

* **URL**

  https://laravel.jdamiancabello.es/api/appointments

* **Method:**

  `GET`

* **Data Params**

  *Headers* <br/>
  Accept	application/json <br/>
  Authorization	Bearer eyJ0eXAi ... <br/>

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    [
      {
        "id": 51,
        "created_at": "2019-03-05 00:36:46",
        "updated_at": "2019-03-05 00:36:46",
        "day": "2019-03-06",
        "timestart": 10,
        "timefinish": 11,
        "user_id": 1
      }
    ]
    ```

**Show appointment**
----

* **URL**

  https://laravel.jdamiancabello.es/api/appointments/ID

* **Method:**

  `GET`

* **Data Params**

  *Headers* <br/>
  Accept	application/json <br/>
  Authorization	Bearer eyJ0eXAi ... <br/>

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    [
      {
        "id": 51,
        "created_at": "2019-03-05 00:36:46",
        "updated_at": "2019-03-05 00:36:46",
        "day": "2019-03-06",
        "timestart": 10,
        "timefinish": 11,
        "user_id": 1
      }
    ]
    ```

* **Error Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
      "message": "Resource not found"
    }
    ```

**Edit appointment**
----

* **URL**

  https://laravel.jdamiancabello.es/api/appointments/ID

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
   `day=[date(dd-mm-yyyy)]`

   `timestart=[integer]`
   
   `timefinish=[integer]` 

* **Data Params**

    Headers

    Content-Type application/x-www-form-urlencoded

    Accept application/json

    Authorization Bearer eyJ0eXAiOiJKV1QiLCJh . . .

    Body

    x-www-form-urlencoded

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    [
      {
        "id": 51,
        "created_at": "2019-03-05 00:36:46",
        "updated_at": "2019-03-05 00:36:46",
        "day": "2019-03-06",
        "timestart": 10,
        "timefinish": 11,
        "user_id": 1
      }
    ]
    ```

**Add appointment**
----

* **URL**

  https://laravel.jdamiancabello.es/api/appointments

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
   `day=[date(dd-mm-yyyy)]`

   `timestart=[integer]`
   
   `timefinish=[integer]` 

* **Data Params**

    Headers

    Content-Type application/x-www-form-urlencoded

    Accept application/json

    Authorization Bearer eyJ0eXAiOiJKV1QiLCJh . . .

    Body

    x-www-form-urlencoded

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** JSON de la reserva

**Delete appointment**
----

* **URL**

  https://laravel.jdamiancabello.es/api/appointments/ID

* **Method:**

  `DELETE`

* **Data Params**

    Headers

    Content-Type application/x-www-form-urlencoded

    Accept application/json

    Authorization Bearer eyJ0eXAiOiJKV1QiLCJh . . .

    Body

    x-www-form-urlencoded

* **Success Response:**

  * **Code:** 204 <br />
    **Content:** Nada

* **Error Response:**

  * **Code:** 404 <br />
    **Content:**
    ```
    {
      "message": "Resource not found"
    }
    ```
