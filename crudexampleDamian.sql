-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-06-2019 a las 23:46:16
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crudexampleDamian`
--
CREATE DATABASE IF NOT EXISTS `crudexampleDamian` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `crudexampleDamian`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appointments`
--

DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `day` date NOT NULL,
  `timestart` int(11) NOT NULL,
  `timefinish` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Disparadores `appointments`
--
DROP TRIGGER IF EXISTS `bi_appointments`;
DELIMITER $$
CREATE TRIGGER `bi_appointments` BEFORE INSERT ON `appointments` FOR EACH ROW begin

    declare msg varchar(128);
    
if new.timestart > new.timefinish then
        set msg = '#ERROR: La hora de inicio no puede ser mayor que la hora final. AM@';
        signal sqlstate '47000' set message_text = msg;
    end if;

    if new.timestart < 9 then
        set msg = '#ERROR: La hora de inicio no puede ser inferior a 9 AM@';        signal sqlstate '47000' set message_text = msg;
    end if;

    if new.timestart > 22 then
        set msg = '#ERROR: El tiempo de entrada no puede ser menor a 10 PM@';
        signal sqlstate '47001' set message_text = msg;
    end if;

    if new.timefinish > 22 then
        set msg = '#ERROR: El tiempo de llegada no puede ser mayor a 10:59 PM@';        signal sqlstate '47002' set message_text = msg;
    end if;

    if DATE_ADD(now(), INTERVAL 7 DAY) < new.day then
        set msg = '#ERROR: No se puede reservar con más de 7 días de antelación@';
        signal sqlstate '47004' set message_text = msg;
    end if;

    if (curdate() > new.day or (curdate() = new.day and new.timestart < hour(now()))) then
        set msg = '#ERROR: No se puede reservar el pasado@';        signal sqlstate '47005' set message_text = msg;
    end if;

    if EXISTS (SELECT day FROM appointments WHERE user_id = new.user_id and day = new.day) then
        set msg = '#ERROR: Solo puede haber una cita por usuario una vez al día, intente con otro día@';
        signal sqlstate '47006' set message_text = msg;
    end if;

    if (new.timefinish - new.timestart) > 1 then
        set msg = '#ERROR: No se permite reservar más de dos horas@';        signal sqlstate '47007' set message_text = msg;
    end if;

    if EXISTS (SELECT * FROM appointments WHERE (timestart between new.timestart and new.timefinish and day = new.day)) then
        set msg = '#ERROR: Hay otra reserva dentro de este rango de tiempo en ese día';
        signal sqlstate '47006' set message_text = msg;
    end if;

    if EXISTS (SELECT * FROM appointments WHERE (new.timestart between timestart and timefinish and day = new.day)) then
        set msg = '#ERROR: Hay otra reserva dentro de este rango de tiempo en ese día';        signal sqlstate '47006' set message_text = msg;
    end if;

end
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `bu_appointments`;
DELIMITER $$
CREATE TRIGGER `bu_appointments` BEFORE UPDATE ON `appointments` FOR EACH ROW begin

    declare msg varchar(128);
    
if new.timestart > new.timefinish then
        set msg = '#ERROR: La hora de inicio no puede ser mayor que la hora final.@';
        signal sqlstate '47000' set message_text = msg;
    end if;

    if new.timestart < 9 then
        set msg = '#ERROR: La hora de inicio no puede ser inferior a 9 AM@';        signal sqlstate '47000' set message_text = msg;
    end if;

    if new.timestart > 22 then
        set msg = '#ERROR: La hora de inicio no puede ser superior a 10 PM@';
        signal sqlstate '47001' set message_text = msg;
    end if;

    if new.timefinish > 22 then
        set msg = '#ERROR: El tiempo de llegada no puede ser mayor a 10:59 PM@';        signal sqlstate '47002' set message_text = msg;
    end if;

    if DATE_ADD(now(), INTERVAL 7 DAY) < new.day then
        set msg = '#ERROR: No se permiten reservas de mas de 7 días de antelación.@';
        signal sqlstate '47004' set message_text = msg;
    end if;

    if (curdate() > new.day or (curdate() = new.day and new.timestart < hour(now()))) then
        set msg = '#ERROR: No se puede registrar una reserva con fecha pasada.@';        signal sqlstate '47005' set message_text = msg;
    end if;

    if EXISTS (SELECT day FROM appointments WHERE user_id = new.user_id and day = new.day and old.day <> new.day) then
        set msg = '#Date error: Solo puede haber una reserva por usuario una vez al día, intente con otro día@';
        signal sqlstate '47006' set message_text = msg;
    end if;

    if (new.timefinish - new.timestart) > 1 then
        set msg = '#ERROR: No puedes reservar más de 2 horas@';
        signal sqlstate '47007' set message_text = msg;
    end if;

    if EXISTS (SELECT * FROM appointments WHERE (timestart between new.timestart and new.timefinish and day = new.day)) then
        set msg = '#ERROR: Hay otra reserva dentro de este rango de tiempo en ese día';        signal sqlstate '47006' set message_text = msg;
    end if;

    if EXISTS (SELECT * FROM appointments WHERE (new.timestart between timestart and timefinish and day = new.day)) then
        set msg = '#ERROR: Hay otra reserva dentro de este rango de tiempo en ese día';
        signal sqlstate '47006' set message_text = msg;
    end if;

end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `appointments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
