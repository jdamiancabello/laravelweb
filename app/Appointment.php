<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = ['day', 'timestart', 'timefinish', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
