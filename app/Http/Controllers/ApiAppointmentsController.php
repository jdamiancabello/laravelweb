<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Appointment;
use Validator;

class ApiAppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(auth()->user()->appointments, 200);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'day' => 'required|date',
                    'timestart' => 'required|integer|min:9|max:22',
                    'timefinish' => 'required|integer|min:10|max:23',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->getMessages();
            $status = 422;
        } else {
            $appo = new Appointment();
            $appo->day = $request->day;
            $appo->timestart = $request->timestart;
            $appo->timefinish = $request->timefinish;

            if (auth()->user()->appointments()->save($appo)) {
                $message = $appo;
                $status = 201;
            } else {
                $message = 'Appointment cannot be added';
                $status = 500;
            }
        }

        return response()->json($message, $status);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appo = auth()->user()->appointments()->find($appo);

        if (!$appo) {
            $message = 'Appointment not found';
            $status = 404;
        } else {
            $message['appointment'] = $appo;
            $status = 200;
        }

        return response()->json($message, $status); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        $appointment = auth()->user()->appointments()->find($appointment->id);

        if (!$appointment) {
            $message = 'Appointment not found';
            $status = 404;
        } else {
            $validator = Validator::make($request->all(), [
                        'day' => 'required|date',
                        'timestart' => 'required|integer',
                        'timefinish' => 'required|integer',
            ]);

            if ($validator->fails()) {
                $message = $validator->errors()->getMessages();
                $status = 422;
                //return response()->json( $validator->errors()->getMessages(), 422);
            } else {
                $updated = $appointment->update($request->all());
                //$updated = $appointment->fill($request->all())->save();

                if ($updated) {
                    $message = $appointment;
                    $status = 201;
                } else {
                    $message = 'The appoontment ' . $appointment->id . ' could not be edited';
                    $status = 500;
                }
            }
        }

        return response()->json($message, $status);     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {       
        $appointment = auth()->user()->appointments()->find($appointment->id);

        if (!$appointment) {
            $message = 'Appointment not found';
            $status = 404;
        } else {
            if ($appointment->delete()) {
                $message = null;
                $status = 204;
            } else {
                $message = 'Appointment cannot be deleted';
                $status = 500;
            }
        }

        return response()->json($message, $status);
    }
}
