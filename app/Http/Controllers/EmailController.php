<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewEmail;
use Validator;
use Exception;

class EmailController extends Controller
{
    public function send(Request $request) {
        $validator = Validator::make($request->all(), [
            'to' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            $message = $validator->errors()->getMessages();
            $status = 501;
        } else {
                $email =  $request->json()->all();
                $to = $email['to'];
                $subject = $email['subject'];
                $content = $email['message'];

                try {
                    Mail::to($to)->send(new NewEmail($subject, $content));
                    $message = "Email enviado";
                    $status = 200;
                } catch (Exception $exception) {
                    $status = 554;
                    $message = "Email no enviado. Error: " . $exception->getMessage();
                }
            }

      return response()->json($message, $status);      
    }
}
