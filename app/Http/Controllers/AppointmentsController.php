<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Appointment;
use Exception;
use App\Mail\NewAppointmentEmail;
use Illuminate\Support\Facades\Mail;

class AppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointments = Appointment::where('user_id', Auth::user()->id)->get();
        return view('appointments.index', compact('appointments'));
    }
public function invitation() { return view('appointments.invite', compact('appointment'));}
    /**
    * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('appointments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'day'=>'required|date',
        'timestart'=> 'required|integer',
        'timefinish' => 'required|integer'
      ]);

      $appointment = new Appointment([
        'day' => $request->get('day'),
        'timestart'=> $request->get('timestart'),
        'timefinish'=> $request->get('timefinish'),
        'user_id' => Auth::id()
      ]);

      try {
      $appointment->save();
      $status = "success";
      $message = 'Reserva creada.';
      } catch (Exception $e) {
      	$status = "error";
	$str = $e->getMessage(); $from_char = '#'; $to_char = '@';
	$from_pos = strpos( $str, $from_char );
	$blank_space_pos = strpos( $str, ' ',  $from_pos );
	$to_pos = strpos( $str, $to_char, $blank_space_pos );
	$message = substr( $str, $from_pos + 1, $to_pos - $from_pos - 1);
      }

      $to = Auth::user()->email;
      try {
            Mail::to($to)->send(new NewAppointmentEmail($appointment));
            $message = $message . " y se le ha enviado un correo";
      } catch (Exception $exception) {
          $status = "error";
          $message = $message . ', y no se le envió el correo. Error: ' . $exception->getMessage();              
      }

      return redirect('/appointments')->with($status, $message);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $appointment = Appointment::findOrFail($id);

        return view('appointments.edit', compact('appointment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'day'=>'required|date',
        'timestart'=> 'required|integer',
        'timefinish' => 'required|integer'
      ]);

      $appo = Appointment::findOrFail($id);
      $appo->day = $request->get('day');
      $appo->timestart = $request->get('timestart');
      $appo->timefinish = $request->get('timefinish');

      try {
      $appo->save();
      $status = "success";
      $message = 'La reserva se editó correctamente.';
      } catch (Exception $e) {
        $status = "error";
        $str = $e->getMessage(); $from_char = '#'; $to_char = '@';
        $from_pos = strpos( $str, $from_char );
        $blank_space_pos = strpos( $str, ' ',  $from_pos );
        $to_pos = strpos( $str, $to_char, $blank_space_pos );
        $message = substr( $str, $from_pos + 1, $to_pos - $from_pos - 1);
      }
      return redirect('/appointments')->with($status, $message);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $appo = Appointment::find($id);
     $appo->delete();

     return redirect('/appointments')->with('success', 'La reserva ha sido eliminada.');
    }
}
