<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'PassportController@login');
//ruta: api/login
Route::post('register', 'PassportController@register');
//ruta: api/register

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@getDetails');
    //ruta: api/user
    Route::resource('appointments', 'ApiAppointmentsController');
    //ruta: api/sites
    Route::get('logout', 'PassportController@logout');
    //ruta: api/logout
});

Route::post('/email', 'EmailController@send');
